Napraviti virtualno okruženje i pokrenuti naredbe za instalaciju:
```
python -m pip install jupyterlab
python -m pip install pandas
python -m pip install matplotlib
python -m pip install seaborn
python -m pip install scikit-learn
```